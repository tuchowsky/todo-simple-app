import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { ContactComponent } from './components/contact/contact.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { TodoAppModule } from './components/todo-app/todo-app.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    // jeżeli dodajemy komponent ręcznie, musimy pamiętać o tym aby dodać jego import do tablicy z deklaracjami
    GalleryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // dodanie osobnego modułu do głównego modułu app.module
    TodoAppModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
