import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import * as Components from './components/index';
// import { HomeComponent } from './components/home/home.component';
import { TodoAppComponent } from './components/todo-app/todo-app.component';
// import { GalleryComponent } from './components/gallery/gallery.component';
// import { ContactComponent } from './components/contact/contact.component';

// routing aplikacji
// definiowane path/ścieźki oraz component, który ma być renderowany
const routes: Routes = [
  {path: 'home', component: Components.HomeComponent},
  {path: 'todo-app', component: TodoAppComponent},
  {path: 'gallery', component: Components.GalleryComponent},
  {path: 'contact', component: Components.ContactComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
