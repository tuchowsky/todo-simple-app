import { Component } from '@angular/core';

@Component({
    // selector/nazwa po ktorej będziemy się odwoływać w HTML
    selector: 'app-gallery',
    templateUrl: './gallery.component.html',
    styleUrls: ['./gallery.component.scss']
})

export class GalleryComponent {
    constructor () {}
}