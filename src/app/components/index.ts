export { HomeComponent } from '../components/home/home.component';
export { GalleryComponent } from '../components/gallery/gallery.component';
export { ContactComponent } from '../components/contact/contact.component';